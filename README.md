<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.aedev V0.3.23 -->
<!-- THIS FILE IS EXCLUSIVELY MAINTAINED by the project aedev.tpl_namespace_root V0.3.14 -->
# setup_project 0.3.22

[![GitLab develop](https://img.shields.io/gitlab/pipeline/aedev-group/aedev_setup_project/develop?logo=python)](
    https://gitlab.com/aedev-group/aedev_setup_project)
[![LatestPyPIrelease](
    https://img.shields.io/gitlab/pipeline/aedev-group/aedev_setup_project/release0.3.21?logo=python)](
    https://gitlab.com/aedev-group/aedev_setup_project/-/tree/release0.3.21)
[![PyPIVersions](https://img.shields.io/pypi/v/aedev_setup_project)](
    https://pypi.org/project/aedev-setup-project/#history)

>aedev_setup_project module 0.3.22.

[![Coverage](https://aedev-group.gitlab.io/aedev_setup_project/coverage.svg)](
    https://aedev-group.gitlab.io/aedev_setup_project/coverage/index.html)
[![MyPyPrecision](https://aedev-group.gitlab.io/aedev_setup_project/mypy.svg)](
    https://aedev-group.gitlab.io/aedev_setup_project/lineprecision.txt)
[![PyLintScore](https://aedev-group.gitlab.io/aedev_setup_project/pylint.svg)](
    https://aedev-group.gitlab.io/aedev_setup_project/pylint.log)

[![PyPIImplementation](https://img.shields.io/pypi/implementation/aedev_setup_project)](
    https://gitlab.com/aedev-group/aedev_setup_project/)
[![PyPIPyVersions](https://img.shields.io/pypi/pyversions/aedev_setup_project)](
    https://gitlab.com/aedev-group/aedev_setup_project/)
[![PyPIWheel](https://img.shields.io/pypi/wheel/aedev_setup_project)](
    https://gitlab.com/aedev-group/aedev_setup_project/)
[![PyPIFormat](https://img.shields.io/pypi/format/aedev_setup_project)](
    https://pypi.org/project/aedev-setup-project/)
[![PyPILicense](https://img.shields.io/pypi/l/aedev_setup_project)](
    https://gitlab.com/aedev-group/aedev_setup_project/-/blob/develop/LICENSE.md)
[![PyPIStatus](https://img.shields.io/pypi/status/aedev_setup_project)](
    https://libraries.io/pypi/aedev-setup-project)
[![PyPIDownloads](https://img.shields.io/pypi/dm/aedev_setup_project)](
    https://pypi.org/project/aedev-setup-project/#files)


## installation


execute the following command to install the
aedev.setup_project module
in the currently active virtual environment:
 
```shell script
pip install aedev-setup-project
```

if you want to contribute to this portion then first fork
[the aedev_setup_project repository at GitLab](
https://gitlab.com/aedev-group/aedev_setup_project "aedev.setup_project code repository").
after that pull it to your machine and finally execute the
following command in the root folder of this repository
(aedev_setup_project):

```shell script
pip install -e .[dev]
```

the last command will install this module portion, along with the tools you need
to develop and run tests or to extend the portion documentation. to contribute only to the unit tests or to the
documentation of this portion, replace the setup extras key `dev` in the above command with `tests` or `docs`
respectively.

more detailed explanations on how to contribute to this project
[are available here](
https://gitlab.com/aedev-group/aedev_setup_project/-/blob/develop/CONTRIBUTING.rst)


## namespace portion documentation

information on the features and usage of this portion are available at
[ReadTheDocs](
https://aedev.readthedocs.io/en/latest/_autosummary/aedev.setup_project.html
"aedev_setup_project documentation").
